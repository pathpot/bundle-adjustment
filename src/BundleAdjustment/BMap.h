#ifndef _PS_BMAP_H_
#define _PS_BMAP_H_

namespace ps{
class BMap{
public:
	~BMap(){
		delete[] _landmark;
	}

	bool initialize(const char *filename){
		std::fstream ifs(filename);
		if (!ifs){
			printf("cannot open file %s\n",filename);
			return false;
		}

		std::stringstream iss;
		iss << ifs.rdbuf();
		ifs.close();

		iss >> _n_landmarks;
		_landmark = new double[_n_landmarks*LANDMARK_DIM];

		for(int i=0;i<_n_landmarks*LANDMARK_DIM;i++)
			iss >> _landmark[i];


		return true;
	}

	bool initialize(int n_landmarks){
		_n_landmarks = n_landmarks;
		_landmark = new double[n_landmarks*LANDMARK_DIM];

		for(int i=0;i<n_landmarks*LANDMARK_DIM;i++)
			_landmark[i] = 32.2;
		return true;
	}

	double* get_landmark(int i){
		return _landmark + LANDMARK_DIM*i;
	}

	int get_n_landmarks(){
		return _n_landmarks;
	}
private:
	int _n_landmarks;
	double *_landmark;
};
}


#endif
