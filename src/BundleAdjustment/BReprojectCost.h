//#ifndef _PS_BREPROJECTCOST_H_
//#define _PS_BREPROJECTCOST_H_
//
//#include "ceres/ceres.h"
//#include "ceres/rotation.h"
//#include "../Common/Common.h"
//#include "../Camera/Camera.h"
//
//namespace ps{
//class BReprojectCost{
//public:
//	// Destructor
//	~BReprojectCost(){
//	}
//
//	// Constructor
//	BReprojectCost(double *measurement, CameraParam *cam_param) : measurement(measurement), _cam_param(cam_param){
//	}
//
//	// Cost Function
//	template <typename T>
//	bool operator()(const T* const camera, const T* const landmark, T* residuals) const {
//		// Transform landmark into camera frame
//		T p[3];
//
//
////		T Rp[9];
////		ceres::MatrixAdapter<T, 3, 3> R(Rp);
////		ceres::EulerAnglesToRotationMatrix(camera, R);
////		p[0] = R(0, 0)*landmark[0] + R(0, 1)*landmark[1] + R(0, 2)*landmark[2];
////		p[1] = R(1, 0)*landmark[0] + R(1, 1)*landmark[1] + R(1, 2)*landmark[2];
////		p[2] = R(2, 0)*landmark[0] + R(2, 1)*landmark[1] + R(2, 2)*landmark[2];
//		ceres::AngleAxisRotatePoint(camera, landmark, p);
//
//		p[0] += camera[3];
//		p[1] += camera[4];
//		p[2] += camera[5];
//
//		// Project local landmark onto image plane
//
//		//project onto camera plane
//		T xp = (T(_cam_param->fx) * p[0] + T(_cam_param->s) * p[1]) / p[2] + T(_cam_param->cx);
//		T yp = (T(_cam_param->fy) * p[1]) / p[2] + T(_cam_param->cy);
//
//		//distort
//		T x0 = (_cam_param->fx * p[0] + _cam_param->s * p[1]) / (_cam_param->fx * p[2]);
//		T y0 = p[1] / p[2];
//		T r2 = x0 * x0 + y0 * y0;
//		xp = xp * (T(1) + _cam_param->k1 * r2 + _cam_param->k2 * r2 * r2);
//		yp = yp * (T(1) + _cam_param->k1 * r2 + _cam_param->k2 * r2 * r2);
//
//		//calculate error
//		residuals[0] = T(measurement[0]) - xp;
//		residuals[1] = T(measurement[1]) - yp;
//
//		return true;
//	}
//
//private:
//	double* measurement;
//	CameraParam* _cam_param;
//};
//}
//
//#endif

#ifndef _PS_BREPROJECTCOST_H_
#define _PS_BREPROJECTCOST_H_

#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "../Common/Common.h"
#include "../Camera/Camera.h"

namespace ps{
class BReprojectCost{
public:
	// Destructor
	~BReprojectCost(){
	}

	// Constructor
	BReprojectCost(double *measurement, CameraParam *cam_param) : measurement(measurement), _cam_param(cam_param){
	}

	// Cost Function
	template <typename T>
	bool operator()(const T* const camera, const T* const landmark, T* residuals) const {
		// Transform landmark into camera frame
//		T pt[3];

//		T Rp[9];
//		ceres::MatrixAdapter<T, 3, 3> R(Rp);
//		ceres::EulerAnglesToRotationMatrix(camera, R);
//		p[0] = R(0, 0)*landmark[0] + R(0, 1)*landmark[1] + R(0, 2)*landmark[2];
//		p[1] = R(1, 0)*landmark[0] + R(1, 1)*landmark[1] + R(1, 2)*landmark[2];
//		p[2] = R(2, 0)*landmark[0] + R(2, 1)*landmark[1] + R(2, 2)*landmark[2];
//		ceres::AngleAxisRotatePoint(camera, landmark, p);
//
//		p[0] += camera[3];
//		p[1] += camera[4];
//		p[2] += camera[5];

//		pt[0] = landmark[0] - camera[3];
//		pt[1] = landmark[1] - camera[4];
//		pt[2] = landmark[2] - camera[5];
//
//
//		std::cout << "[landmark]" << std::endl;
//		std::cout << landmark[0] << std::endl;
//		std::cout << landmark[1] << std::endl;
//		std::cout << landmark[2] << std::endl;
//
//		std::cout << "[camera]" << std::endl;
//		std::cout << camera[3] << std::endl;
//		std::cout << camera[4] << std::endl;
//		std::cout << camera[5] << std::endl;
//
//		T angle_axis[3], pt2[3];
//		angle_axis[0] = -camera[0];
//		angle_axis[1] = -camera[1];
//		angle_axis[2] = -camera[2];
//		ceres::AngleAxisRotatePoint(angle_axis, pt, pt2);
//
//		T p[3];
//
//		p[0] = pt2[1];
//		p[1] = pt2[0];
//		p[2] = -pt2[2];
//
//		std::cout << "[pt2]" << std::endl;
//		std::cout << pt2[0] << std::endl;
//		std::cout << pt2[1] << std::endl;
//		std::cout << pt2[2] << std::endl;
		T thx = camera[0];
		T thy = camera[1];
		T thz = camera[2];
		T rx = camera[3];
		T ry = camera[4];
		T rz = camera[5];
		T lx = landmark[0];
		T ly = landmark[1];
		T lz = landmark[2];

		T ox2 = -lz*sin(thy)+rz*sin(thy)+lx*cos(thy)*cos(thz)-rx*cos(thy)*cos(thz)+ly*cos(thy)*sin(thz)-ry*cos(thy)*sin(thz);
		T oy2 = -lx*(cos(thx)*sin(thz)-cos(thz)*sin(thx)*sin(thy))+ly*(cos(thx)*cos(thz)+sin(thx)*sin(thy)*sin(thz))+rx*(cos(thx)*sin(thz)-cos(thz)*sin(thx)*sin(thy))-ry*(cos(thx)*cos(thz)+sin(thx)*sin(thy)*sin(thz))+lz*cos(thy)*sin(thx)-rz*cos(thy)*sin(thx);
		T oz2 = lx*(sin(thx)*sin(thz)+cos(thx)*cos(thz)*sin(thy))-ly*(cos(thz)*sin(thx)-cos(thx)*sin(thy)*sin(thz))-rx*(sin(thx)*sin(thz)+cos(thx)*cos(thz)*sin(thy))+ry*(cos(thz)*sin(thx)-cos(thx)*sin(thy)*sin(thz))+lz*cos(thx)*cos(thy)-rz*cos(thx)*cos(thy);

		T p[3];

		p[0] = ox2;
		p[1] = oy2;
		p[2] = oz2;

		// Project local landmark onto image plane

		//project onto camera plane
		T xp = (T(_cam_param->fx) * p[0] + T(_cam_param->s) * p[1]) / p[2] + T(_cam_param->cx);
		T yp = (T(_cam_param->fy) * p[1]) / p[2] + T(_cam_param->cy);

		//distort
		T x0 = (_cam_param->fx * p[0] + _cam_param->s * p[1]) / (_cam_param->fx * p[2]);
		T y0 = p[1] / p[2];
		T r2 = x0 * x0 + y0 * y0;
		xp = xp * (T(1) + _cam_param->k1 * r2 + _cam_param->k2 * r2 * r2);
		yp = yp * (T(1) + _cam_param->k1 * r2 + _cam_param->k2 * r2 * r2);

		//calculate error
		residuals[0] = T(measurement[0]) - xp;
		residuals[1] = T(measurement[1]) - yp;

//		std::cout << "xm : " << T(measurement[0]) << std::endl;
//		std::cout << "ym : " << T(measurement[1]) << std::endl;
//
//
//		std::cout << "xp : " << xp << std::endl;
//		std::cout << "yp : " << yp << std::endl;
//		getchar();

		return true;
	}

private:
	double* measurement;
	CameraParam* _cam_param;
};
}

#endif
