#ifndef _PS_BUNDLEADJUSTMENT_H_
#define _PS_BUNDLEADJUSTMENT_H_

#include "../Data/MNode.h"
#include "BMap.h"
#include "BReprojectCost.h"
#include "ceres/ceres.h"
#include "ceres/rotation.h"

namespace ps{
class BundleAdjustment{
public:
	BundleAdjustment(CameraParam *camera_param) : camera_param(camera_param){
	}

	void doBundleAdjustment(std::vector<MNode*> *mNodes, BMap *bMap){
		// Build the problem.
		ceres::Problem problem;

		// add residual block for every node
		MNode *current_node;


		int nBlock = 0;
		for(unsigned int n=0;n<mNodes->size();n++){
			current_node = (*mNodes)[n];
			double *camera = current_node->get_camera();
			// loop for every measurement
			for(int m=0;m<current_node->get_n_measurements();m++){
				//get measurement
				double *measurement = current_node->get_measurement(m);

				//get landmark location
				int tag = current_node->get_landmark_tag(m);
				double *landmark = bMap->get_landmark(tag);

				//set cost function
				ceres::CostFunction* cost_fn = new ceres::AutoDiffCostFunction<BReprojectCost, 2, 6, 3>(new BReprojectCost(measurement, camera_param));

				//add block to the problem
				problem.AddResidualBlock(cost_fn, NULL, camera, landmark);
				nBlock++;
			}
		}

		// Run the solver!
		ceres::Solver::Options options;
		options.linear_solver_type = ceres::DENSE_SCHUR;
		options.minimizer_progress_to_stdout = true;
		ceres::Solver::Summary summary;
		ceres::Solve(options, &problem, &summary);

		std::cout << "I : " << sqrt(summary.initial_cost)/nBlock << std::endl;
		std::cout << "N : " << sqrt(summary.final_cost)/nBlock << std::endl;
	}

private:
	CameraParam *camera_param;

};

}


#endif
