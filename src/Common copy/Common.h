#ifndef _PS_COMMON_H_
#define _PS_COMMON_H_


#define NEAR_ZERO 1e-10

#define MEASUREMENT_DIM 2 //[u v]
#define LANDMARK_DIM 3 //[x y z]
#define CAMERA_DIM 6 //[x y z roll pitch yoll]

template <typename, typename> struct is_same { static const bool value = false;};
template <typename T> struct is_same<T,T> { static const bool value = true;};


#endif
