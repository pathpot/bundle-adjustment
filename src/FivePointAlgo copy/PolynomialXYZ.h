#ifndef _PS_POLYNOMIALXYZ3_H_
#define _PS_POLYNOMIALXYZ3_H_

#include <iostream>
#include "../Common/Common.h"

#define PS_POLY_NTERM 30

namespace ps{

//
// 3rd order polynomial of (x, y, z)
//
class PolynomialXYZ{
	double coef[PS_POLY_NTERM];
	static int INDEXTABLE[4][4][5]; //The table which indicates array index for coef[i] for each polynomial degree
	static int COEFTABLE[PS_POLY_NTERM][3]; //degree for x y z
	static bool hasTable;

public:
	//
	// Initialize coefficient table
	//
	static void init();

	PolynomialXYZ(){
		if(!hasTable){
			std::cerr << "Must initialize polynomial first using PolynomialXYZ::init()\n";
		}

		for(int i=0;i<PS_POLY_NTERM;i++){
			coef[i] = 0;
		}
	}


	//
	// Add
	//
	void add(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out){
		for(int i=0;i<PS_POLY_NTERM;i++){
			out->coef[i] = poly0->coef[i] + poly1->coef[i];
		}
	}

	//
	// Minus
	//
	void minus(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out){
		for(int i=0;i<PS_POLY_NTERM;i++){
			out->coef[i] = poly0->coef[i] - poly1->coef[i];
		}
	}

	//
	// Multiply
	//
	void multiply(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out){
		for(int i=0;i<PS_POLY_NTERM;i++){
			out->coef[i] = 0;
		}

		double c0, c1, cout;
		int x0, y0, z0, x1, x1, z1, newIndex;
		for(int i=0;i<PS_POLY_NTERM;i++){
			c0 = poly0->coef[i];
			if(abs(c0) < NEAR_ZERO)
				continue;

			x0 = COEFTABLE[i][0];
			y0 = COEFTABLE[i][1];
			z0 = COEFTABLE[i][2];
			for(int j=0;j<PS_POLY_NTERM;j++){
				c1 = poly1->coef[i];
				if(abs(c1) < NEAR_ZERO)
					break;

				x1 = COEFTABLE[j][0];
				y1 = COEFTABLE[j][1];
				z1 = COEFTABLE[j][2];

				newIndex = INDEXTABLE[x0 + x1][y0 + y1][z0 + z1];
				cout = c0 + c1;
				out->coef[newIndex] = cout;
			}
		}
	}

	//
	// Print object to stream
	//
	friend std::ostream& operator << (std::ostream& os, PolynomialXYZ& m){
		bool blank = true;
		for(int c=29;c>=0;c--){
			if(abs(coef[c]) > NEAR_ZERO){
				if(blank == false){
					os << "+ ";
				}

				blank = false;

				os << coef[c];
//				for(int k=0;k<COEFTABLE[c][0];k++){
//					os << "x";
//				}
//
//				for(int k=0;k<COEFTABLE[c][1];k++){
//					os << "y";
//				}
//
//				for(int k=0;k<COEFTABLE[c][2];k++){
//					os << "z";
//				}

				os << " ";
			}
		}

		if(blank)
			os << "0";

		os << std::endl;
		return os;
	}

private:


	// change degree of polynomial into index
	int coefIndex(int xd, int yd, int zd){
		if(xd < 0 || yd < 0 || zd < 0 || xd + yd + zd > 3){
			std::cerr << "incorrect degree!\n";
		}

		return COEFTABLE[xd][yd][zd];
	}

};
}

#endif
