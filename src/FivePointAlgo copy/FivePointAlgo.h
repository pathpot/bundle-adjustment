/*
 * The method of five point algorithm in this work is implemented using the algorithm from
 * "An Efficient Solution to the Five-Point Relative Pose Problem by David Nister(2004)"
 */

#ifndef _PS_FIVEPOINTALGO_H_
#define _PS_FIVEPOINTALGO_H_

#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/SVD"
#include "eigen3/unsupported/Eigen/Polynomials"

#include "../Camera/CameraParam.h"
#include "../Data/MNode.h"

#include "PolynomialXYZ.h"

#define NSELPOINTS 5

namespace ps{

class FivePointAlgo{
public:
	FivePointAlgo(){
		PolynomialXYZ::init();
	}

	//
	// adjust camera position in mNode using five-point algorithm
	//
	void adjustRelativePose(MNode *mNode0, MNode *mNode1, CameraParam *cam_param){
		// get number of points
		int n0 = mNode0->get_n_measurements();
		int n1 = mNode1->get_n_measurements();

		// calculate inverse of K matrix
		Eigen::MatrixXd K(3, 3);
		K << cam_param->fx, cam_param->s, cam_param->cx,
				0, cam_param->fy, cam_param->cy,
				0, 0, 1;
		Eigen::MatrixXd Kinv = K.inverse();

		// project pixel points of node 0 with K inverse
		Eigen::MatrixXd normPoint0(n0, MEASUREMENT_DIM + 1); //homogeneous representation
		_normalizePoints(mNode0, Kinv, normPoint0);

		// project pixel points of node 1 with K inverse
		Eigen::MatrixXd normPoint1(n1, MEASUREMENT_DIM + 1); //homogeneous representation
		_normalizePoints(mNode1, Kinv, normPoint1);

		//RANSAC for five-point algorithm loops
		Eigen::MatrixXd Q(5, 9), R(3, 3);
		Eigen::Vector3d t;
		int selectedIndex0[NSELPOINTS], selectedIndex1[NSELPOINTS];
		int selectedTag[NSELPOINTS];
		while(true){
			//TODO random common tag
			for(int i=0;i<NSELPOINTS;i++)
				selectedTag[i] = i;

			//TODO set selected indices
			for(int i=0;i<NSELPOINTS;i++)
				selectedIndex0[i] = i;

			for(int i=0;i<NSELPOINTS;i++)
				selectedIndex1[i] = i;

			//set  5x9 matrix of Q
			for(int i=0;i<NSELPOINTS;i++){
				Q.row(i) << normPoint0.row(selectedIndex0[i])(0)*normPoint1.row(selectedIndex1[i])(0),
						normPoint0.row(selectedIndex0[i])(1)*normPoint1.row(selectedIndex1[i])(0),
						normPoint0.row(selectedIndex0[i])(2)*normPoint1.row(selectedIndex1[i])(0),
						normPoint0.row(selectedIndex0[i])(0)*normPoint1.row(selectedIndex1[i])(1),
						normPoint0.row(selectedIndex0[i])(1)*normPoint1.row(selectedIndex1[i])(1),
						normPoint0.row(selectedIndex0[i])(2)*normPoint1.row(selectedIndex1[i])(1),
						normPoint0.row(selectedIndex0[i])(0)*normPoint1.row(selectedIndex1[i])(2),
						normPoint0.row(selectedIndex0[i])(1)*normPoint1.row(selectedIndex1[i])(2),
						normPoint0.row(selectedIndex0[i])(2)*normPoint1.row(selectedIndex1[i])(2);
			}

			//solve rotation & translation
			solveRelativePose(Q, R, t);

			//TODO check termination criteria

			break;

		}
	}

private:
	void solveRelativePose(Eigen::MatrixXd &Q, Eigen::MatrixXd &R, Eigen::Vector3d &t){
		// 1. Estimate null space of Q for X Y Z W (E = xX + yY + zZ + wW)
		Eigen::JacobiSVD<Eigen::MatrixXd> svdQ(Q, Eigen::ComputeFullV);
		Eigen::VectorXd X(9), Y(9), Z(9), W(9);
		X << svdQ.matrixV().col(5);
		Y << svdQ.matrixV().col(6);
		Z << svdQ.matrixV().col(7);
		W << svdQ.matrixV().col(8);

		// 2. Expansion of the cubic constraints
		PolynomialXYZ poly;
		std::cout << poly;

		// 3. Gauss-Jordan elimination with partial pivoting on the 10x20 matrix
		// 4. Extraction of roots from the tenth degree polynomial
		// 5. Recovery of R and t corresponding to each real root
	}

	//
	// multiply each node by inverse of intrinsic matrix
	//
	void _normalizePoints(MNode *node, Eigen::MatrixXd &Kinv, Eigen::MatrixXd &normPoints){
		if(normPoints.rows() != node->get_n_measurements() || normPoints.cols() != MEASUREMENT_DIM + 1){
			std::cerr << "Incompatible normPoints dimension\n";
			return;
		}

		Eigen::Vector3d oldVect;
		for(int p=0;p<node->get_n_measurements();p++){
			oldVect << node->get_measurement(p)[0],
					node->get_measurement(p)[1],
					1;

			normPoints.row(p) = Kinv*oldVect;
		}


	}
};

}


#endif
