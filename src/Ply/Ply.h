#ifndef PSPLY_H_
#define PSPLY_H_

#include "rply.h"
#include <cstddef>

namespace ps{

class Ply {
public:
	static bool write_ply(const char *file_name, double *points, int nPoints){

	    // create output file
	    p_ply oply;
	    oply = ply_create(file_name, PLY_ASCII, NULL, 0, NULL);
	    if (!oply)
	    	return false;

	    // add element
	    if (!ply_add_element(oply, "vertex", nPoints))
	    	return 0;

	    // add properties
	    ply_add_property(oply, "x", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT);
	    ply_add_property(oply, "y", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT);
	    ply_add_property(oply, "z", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT);

	    // write header
	    if (!ply_write_header(oply))
	    	return false;

	    // write point data
	    for(int i=0;i<nPoints;i++){
	    	ply_write(oply, points[3*i + 0]);
	    	ply_write(oply, points[3*i + 1]);
	    	ply_write(oply, points[3*i + 2]);
	    }

	    // close ply file
	    if (!ply_close(oply))
	    	return false;

		return true;
	}
};

}

#endif
