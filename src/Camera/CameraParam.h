#ifndef _PS_CAMERAPARAM_H_
#define _PS_CAMERAPARAM_H_

#include<iostream>
#include<fstream>

namespace ps{
class CameraParam{
public:
	double fx, fy, cx, cy, s; //intrinsic parameter
	double k1, k2; //radial distortion coefficients
	bool loadFile(const char* filename){
		std::ifstream file;
		file.open(filename);
		if(!file.is_open()){
			std::cerr << "cannot open file " << filename;
			return false;
		}

		file >> fx;
		file >> fy;
		file >> cx;
		file >> cy;
		file >> s;
		file >> k1;
		file >> k2;

		file.close();
		return true;
	}

};
}
#endif
