#ifndef _PS_CAMERA_H_
#define _PS_CAMERA_H_

#include "CameraParam.h"

namespace ps{

//Please note that output2D here has the origin at bottom left
class Camera{
public:
	static void projectPoint(double* input, CameraParam *param, double *output2D){
		//project onto camera plane
		double xp = (param->fx * input[0] + param->s * input[1]) / input[2] + param->cx;
		double yp = (param->fy * input[1]) / input[2] + param->cy;

		//distort
		double x0 = (param->fx * input[0] + param->s * input[1]) / (param->fx * input[2]);
		double y0 = input[1] / input[2];
		double r2 = x0 * x0 + y0 * y0; //xp * xp + yp * yp;
		output2D[0] = xp * (1 + param->k1 * r2 + param->k2 * r2 * r2);
		output2D[1] = yp * (1 + param->k1 * r2 + param->k2 * r2 * r2);
	}
};
}


#endif
