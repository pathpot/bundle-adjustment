#ifndef _PS_TRIANGULATION_H_
#define _PS_TRIANGULATION_H_

#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/SVD"
#include "../Camera/CameraParam.h"
#include "TriangulationCost.h"

namespace ps{

class Triangulation{
public:
	static void estimatePointLocation(double *pixel0, double *pixel1, double **R, double *t, CameraParam *camera_param, double *x){
		// Build the problem.
		ceres::Problem problem;

		// add residual block for every node
		ceres::CostFunction* cost_fn = new ceres::AutoDiffCostFunction<TriangulationCost, 4, 3>(new TriangulationCost(pixel0, pixel1, R, t, camera_param));
		problem.AddResidualBlock(cost_fn, NULL, x);

		// Run the solver!
		ceres::Solver::Options options;
		options.linear_solver_type = ceres::DENSE_SCHUR;
		options.minimizer_progress_to_stdout = true;
		ceres::Solver::Summary summary;
		ceres::Solve(options, &problem, &summary);

	}

};
}


#endif
