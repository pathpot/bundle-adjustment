#ifndef _TRIANGULATIONCOST_H_
#define _TRIANGULATIONCOST_H_

#include "../Common/Common.h"

namespace ps{
class TriangulationCost{
public:
	TriangulationCost(double *pixel0, double *pixel1, double **R, double *t, CameraParam *camera_param) : pixel0(pixel0), pixel1(pixel1), t(t), camera_param(camera_param){
		r = new double[9];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				r[3*i + j] = R[i][j];
			}
		}
	}

	~TriangulationCost(){
		delete[] r;
	}

	// Cost Function
	template <typename T>
	bool operator()(const T* const x, T* residuals) const {
		//First
		T p[3];

		p[0] = x[0];
		p[1] = x[1];
		p[2] = x[2];

		// Project local landmark onto image plane

		//project onto camera plane
		T xp = (T(camera_param->fx) * p[0] + T(camera_param->s) * p[1]) / p[2] + T(camera_param->cx);
		T yp = (T(camera_param->fy) * p[1]) / p[2] + T(camera_param->cy);

		//distort
		T x0 = (camera_param->fx * p[0] + camera_param->s * p[1]) / (camera_param->fx * p[2]);
		T y0 = p[1] / p[2];
		T r2 = x0 * x0 + y0 * y0;
		xp = xp * (T(1) + camera_param->k1 * r2 + camera_param->k2 * r2 * r2);
		yp = yp * (T(1) + camera_param->k1 * r2 + camera_param->k2 * r2 * r2);

		//calculate error
		residuals[0] = T(pixel0[0]) - xp;
		residuals[1] = T(pixel0[1]) - yp;

		//Second
		T r_axis[3];
		ceres::RotationMatrixToAngleAxis((T*)(r), r_axis);
		ceres::AngleAxisRotatePoint(r_axis, x, p);
		p[0] += T(t[0]);
		p[1] += T(t[1]);
		p[2] += T(t[2]);

		// Project local landmark onto image plane

		//project onto camera plane
		xp = (T(camera_param->fx) * p[0] + T(camera_param->s) * p[1]) / p[2] + T(camera_param->cx);
		yp = (T(camera_param->fy) * p[1]) / p[2] + T(camera_param->cy);

		//distort
		x0 = (camera_param->fx * p[0] + camera_param->s * p[1]) / (camera_param->fx * p[2]);
		y0 = p[1] / p[2];
		r2 = x0 * x0 + y0 * y0;
		xp = xp * (T(1) + camera_param->k1 * r2 + camera_param->k2 * r2 * r2);
		yp = yp * (T(1) + camera_param->k1 * r2 + camera_param->k2 * r2 * r2);

		//calculate error
		residuals[2] = T(pixel1[0]) - xp;
		residuals[3] = T(pixel1[1]) - yp;

		return true;
	}

private:
	double *pixel0;
	double *pixel1;
	double *t, *r;
	CameraParam *camera_param;

};

}


#endif
