#ifndef _PS_POLYNOMIALZ_H_
#define _PS_POLYNOMIALZ_H_

#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/SVD"
#include "eigen3/unsupported/Eigen/Polynomials"

#include "../Common/Common.h"
#include <iostream>
#include <math.h>

#define PS_POLYZ_NTERM 11
#define PS_POLYZ_DEGREE 10

namespace ps{
class PolynomialZ{
	double coef[PS_POLYZ_NTERM];

public:
	//
	// Constructor
	//
	PolynomialZ();

	//
	// Add
	//
	static void add(PolynomialZ *poly0, PolynomialZ *poly1, PolynomialZ *out);

	//
	// Minus
	//
	static void minus(PolynomialZ *poly0, PolynomialZ *poly1, PolynomialZ *out);

	//
	// Multiply
	//
	static void multiply(PolynomialZ *poly0, PolynomialZ *poly1, PolynomialZ *out);
	static void multiply(PolynomialZ *poly0, double k, PolynomialZ *out);

	//
	// Substitute
	//
	double substituteVar(double z);

	//
	// Print object to stream
	//
	friend std::ostream& operator << (std::ostream& os, PolynomialZ& m){
		bool blank = true;
		for(int zd=PS_POLYZ_NTERM-1;zd>=0;zd--){
			if(fabs(m.coef[zd]) > NEAR_ZERO){
				if(blank == false){
					os << "+ ";
				}

				blank = false;

				os << "(" << m.coef[zd] << ")";

				if(zd == 1){
					os << 'z';
				}else if(zd > 1){
					os << 'z' << zd;
				}

				os << " ";
			}
		}

		if(blank)
			os << "0";

		return os;
	}

	double& operator()(const int zd){
	    return coef[zd];
	}

};
}




#endif
