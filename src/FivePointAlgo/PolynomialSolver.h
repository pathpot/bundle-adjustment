// Polynomial Solver Class
// Code From http://www.akiti.ca/rpoly_ak1_Intro.html
#ifndef _PS_POLYNOMIALSOLVER_H_
#define _PS_POLYNOMIALSOLVER_H_

#include <iostream>
#include <fstream>
#include <cctype>
#include <cmath>
#include <cfloat>

#define PS_POLYSOLVER_MAXDEGREE 100
#define PS_POLYSOLVER_MDP1 PS_POLYSOLVER_MAXDEGREE+1

namespace ps{
class PolynomialSolver{
public:
	//
	// coef[i] -> x^i
	//
	static void solvePoly(double *coef, int degree, double *rootR, double *rootI);
private:
	static void rpoly_ak1(double op[PS_POLYSOLVER_MDP1], int* Degree, double zeror[PS_POLYSOLVER_MAXDEGREE], double zeroi[PS_POLYSOLVER_MAXDEGREE]);
	static void Fxshfr_ak1(int L2, int* NZ, double sr, double v, double K[PS_POLYSOLVER_MDP1], int N, double p[PS_POLYSOLVER_MDP1], int NN, double qp[PS_POLYSOLVER_MDP1], double u, double* lzi, double* lzr, double* szi, double* szr);
	static void QuadSD_ak1(int NN, double u, double v, double p[PS_POLYSOLVER_MDP1], double q[PS_POLYSOLVER_MDP1], double* a, double* b);
	static int calcSC_ak1(int N, double a, double b, double* a1, double* a3, double* a7, double* c, double* d, double* e, double* f, double* g, double* h, double K[PS_POLYSOLVER_MDP1], double u, double v, double qk[PS_POLYSOLVER_MDP1]);
	static void nextK_ak1(int N, int tFlag, double a, double b, double a1, double* a3, double* a7, double K[PS_POLYSOLVER_MDP1], double qk[PS_POLYSOLVER_MDP1], double qp[PS_POLYSOLVER_MDP1]);
	static void newest_ak1(int tFlag, double* uu, double* vv, double a, double a1, double a3, double a7, double b, double c, double d, double f, double g, double h, double u, double v, double K[PS_POLYSOLVER_MDP1], int N, double p[PS_POLYSOLVER_MDP1]);
	static void QuadIT_ak1(int N, int* NZ, double uu, double vv, double* szr, double* szi, double* lzr, double* lzi, double qp[PS_POLYSOLVER_MDP1], int NN, double* a, double* b, double p[PS_POLYSOLVER_MDP1], double qk[PS_POLYSOLVER_MDP1], double* a1, double* a3, double* a7, double* c, double* d, double* e, double* f, double* g, double* h, double K[PS_POLYSOLVER_MDP1]);
	static void RealIT_ak1(int* iFlag, int* NZ, double* sss, int N, double p[PS_POLYSOLVER_MDP1], int NN, double qp[PS_POLYSOLVER_MDP1], double* szr, double* szi, double K[PS_POLYSOLVER_MDP1], double qk[PS_POLYSOLVER_MDP1]);
	static void Quad_ak1(double a, double b1, double c, double* sr, double* si, double* lr, double* li);
};


}



#endif
