#ifndef _PS_POLYNOMIALXYZ3_H_
#define _PS_POLYNOMIALXYZ3_H_

#include <iostream>
#include "../Common/Common.h"
#include <math.h>

#define PS_POLYXYZ_NTERM 20
#define PS_POLYXYZ_DEGREE 3

namespace ps{

//
// 3rd order polynomial of (x, y, z)
//
class PolynomialXYZ{
	static int INDEXTABLE[4][4][4]; //The table which indicates array index for coef[i] for each polynomial degree
	static int COEFTABLE[PS_POLYXYZ_NTERM][3]; //degree for x y z
	static bool hasTable;

	double coef[PS_POLYXYZ_NTERM];

public:
	//
	// Constructor
	//
	PolynomialXYZ();

	//
	// Initialize coefficient table
	//
	static void init();

	//
	// Add
	//
	static void add(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out);

	//
	// Minus
	//
	static void minus(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out);

	//
	// Multiply
	//
	static void multiply(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out);
	static void multiply(PolynomialXYZ *poly0, double k, PolynomialXYZ *out);

	//
	// Substitute
	//
	double substituteVar(double x, double y, double z);

	//
	// Change degree of polynomial into index
	//
	static int coefIndex(int xd, int yd, int zd);

	//
	// Print object to stream
	//
	friend std::ostream& operator << (std::ostream& os, PolynomialXYZ& m){
		bool blank = true;
		for(int c=0;c<PS_POLYXYZ_NTERM;c++){
			if(fabs(m.coef[c]) > NEAR_ZERO){
				if(blank == false){
					os << "+ ";
				}

				blank = false;

				os << "(" << m.coef[c] << ")";

				char var[3];
				var[0] = 'x';
				var[1] = 'y';
				var[2] = 'z';
				for(int v=0;v<3;v++){
					if(COEFTABLE[c][v] == 1){
						os << char(var[v]);
					}else if(COEFTABLE[c][v] > 1){
						os << char(var[v]) << COEFTABLE[c][v];
					}
				}
				os << " ";
			}
		}

		if(blank)
			os << "0";

		return os;
	}

	double& operator()(const int xd, const int yd, const int zd){
	    return coef[coefIndex(xd, yd, zd)];
	}

	double operator[](const int i){
	    return coef[i];
	}

};
}

#endif
