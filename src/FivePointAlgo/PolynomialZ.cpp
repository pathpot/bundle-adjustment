#include "PolynomialZ.h"


namespace ps{
//
// Constructor
//
PolynomialZ::PolynomialZ(){
	for(int i=0;i<PS_POLYZ_NTERM;i++){
		coef[i] = 0;
	}
}

//
// Add
//
void PolynomialZ::add(PolynomialZ *poly0, PolynomialZ *poly1, PolynomialZ *out){
	for(int i=0;i<PS_POLYZ_NTERM;i++){
		out->coef[i] = poly0->coef[i] + poly1->coef[i];
	}
}

//
// Minus
//
void PolynomialZ::minus(PolynomialZ *poly0, PolynomialZ *poly1, PolynomialZ *out){
	for(int i=0;i<PS_POLYZ_NTERM;i++){
		out->coef[i] = poly0->coef[i] - poly1->coef[i];
	}
}

//
// Multiply
//
void PolynomialZ::multiply(PolynomialZ *poly0, PolynomialZ *poly1, PolynomialZ *out){
	// reset coefficient of output polynomial
	for(int i=0;i<PS_POLYZ_NTERM;i++){
		out->coef[i] = 0;
	}

	double c0, c1;
	for(int z0=0;z0<PS_POLYZ_NTERM;z0++){
		// read coefficient for poly0
		c0 = poly0->coef[z0];

		// if don't have this term -> skip
		if(fabs(c0) < NEAR_ZERO)
			continue;

		for(int z1=0;z1<PS_POLYZ_NTERM;z1++){
			//read coefficient for poly1
			c1 = poly1->coef[z1];

			// if don't have this term -> skip
			if(fabs(c1) < NEAR_ZERO)
				continue;

			// set out data
			out->coef[z0 + z1] += c0*c1;
		}
	}
}

//
// Multiply with double
//
void PolynomialZ::multiply(PolynomialZ *poly0, double k, PolynomialZ *out){
	for(int i=0;i<PS_POLYZ_NTERM;i++){
		out->coef[i] = poly0->coef[i]*k;
	}
}

//
// Substitute Var
//
double PolynomialZ::substituteVar(double z){
	double result = 0;
	for(int i=0;i<PS_POLYZ_NTERM;i++){
		result += coef[i]*pow(z, i);
	}
	return result;
}

} //end namespace

