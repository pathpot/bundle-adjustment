#include "PolynomialXYZ.h"

namespace ps{
int PolynomialXYZ::INDEXTABLE[4][4][4];
int PolynomialXYZ::COEFTABLE[PS_POLYXYZ_NTERM][3];
bool PolynomialXYZ::hasTable = false;

void PolynomialXYZ::init(){
	hasTable = true;

	// x3
	COEFTABLE[0][0] = 3;
	COEFTABLE[0][1] = 0;
	COEFTABLE[0][2] = 0;

	// y3
	COEFTABLE[1][0] = 0;
	COEFTABLE[1][1] = 3;
	COEFTABLE[1][2] = 0;

	// x2y
	COEFTABLE[2][0] = 2;
	COEFTABLE[2][1] = 1;
	COEFTABLE[2][2] = 0;

	// xy2
	COEFTABLE[3][0] = 1;
	COEFTABLE[3][1] = 2;
	COEFTABLE[3][2] = 0;

	// x2z
	COEFTABLE[4][0] = 2;
	COEFTABLE[4][1] = 0;
	COEFTABLE[4][2] = 1;

	// x2
	COEFTABLE[5][0] = 2;
	COEFTABLE[5][1] = 0;
	COEFTABLE[5][2] = 0;

	// y2z
	COEFTABLE[6][0] = 0;
	COEFTABLE[6][1] = 2;
	COEFTABLE[6][2] = 1;

	// y2
	COEFTABLE[7][0] = 0;
	COEFTABLE[7][1] = 2;
	COEFTABLE[7][2] = 0;

	// xyz
	COEFTABLE[8][0] = 1;
	COEFTABLE[8][1] = 1;
	COEFTABLE[8][2] = 1;

	// xy
	COEFTABLE[9][0] = 1;
	COEFTABLE[9][1] = 1;
	COEFTABLE[9][2] = 0;

	// xz2
	COEFTABLE[10][0] = 1;
	COEFTABLE[10][1] = 0;
	COEFTABLE[10][2] = 2;

	// xz
	COEFTABLE[11][0] = 1;
	COEFTABLE[11][1] = 0;
	COEFTABLE[11][2] = 1;


	// x
	COEFTABLE[12][0] = 1;
	COEFTABLE[12][1] = 0;
	COEFTABLE[12][2] = 0;

	// yz2
	COEFTABLE[13][0] = 0;
	COEFTABLE[13][1] = 1;
	COEFTABLE[13][2] = 2;

	// yz
	COEFTABLE[14][0] = 0;
	COEFTABLE[14][1] = 1;
	COEFTABLE[14][2] = 1;

	// y
	COEFTABLE[15][0] = 0;
	COEFTABLE[15][1] = 1;
	COEFTABLE[15][2] = 0;

	// z3
	COEFTABLE[16][0] = 0;
	COEFTABLE[16][1] = 0;
	COEFTABLE[16][2] = 3;

	// z2
	COEFTABLE[17][0] = 0;
	COEFTABLE[17][1] = 0;
	COEFTABLE[17][2] = 2;

	// z
	COEFTABLE[18][0] = 0;
	COEFTABLE[18][1] = 0;
	COEFTABLE[18][2] = 1;

	// 1
	COEFTABLE[19][0] = 0;
	COEFTABLE[19][1] = 0;
	COEFTABLE[19][2] = 0;

	//set index table
	int xd, yd, zd;
	for(int i=0;i< PS_POLYXYZ_NTERM;i++){
		xd = COEFTABLE[i][0];
		yd = COEFTABLE[i][1];
		zd = COEFTABLE[i][2];

		INDEXTABLE[xd][yd][zd] = i;
	}
}

PolynomialXYZ::PolynomialXYZ(){
	if(!hasTable){
		std::cerr << "Must initialize polynomial first using PolynomialXYZ::init()\n";
	}

	for(int i=0;i<PS_POLYXYZ_NTERM;i++){
		coef[i] = 0;
	}
}

void PolynomialXYZ::add(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out){
	for(int i=0;i<PS_POLYXYZ_NTERM;i++){
		out->coef[i] = poly0->coef[i] + poly1->coef[i];
	}
}

void PolynomialXYZ::minus(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out){
	for(int i=0;i<PS_POLYXYZ_NTERM;i++){
		out->coef[i] = poly0->coef[i] - poly1->coef[i];
	}
}

void PolynomialXYZ::multiply(PolynomialXYZ *poly0, PolynomialXYZ *poly1, PolynomialXYZ *out){
	// reset coefficient of output polynomial
	for(int i=0;i<PS_POLYXYZ_NTERM;i++){
		out->coef[i] = 0;
	}

	double c0, c1, cout;
	int x0, y0, z0, x1, y1, z1, newIndex;
	for(int i=0;i<PS_POLYXYZ_NTERM;i++){
		// read coefficient for poly0
		c0 = poly0->coef[i];

		// if don't have this term -> skip
		if(fabs(c0) < NEAR_ZERO)
			continue;

		// degree of this term
		x0 = COEFTABLE[i][0];
		y0 = COEFTABLE[i][1];
		z0 = COEFTABLE[i][2];

		for(int j=0;j<PS_POLYXYZ_NTERM;j++){
			//read coefficient for poly1
			c1 = poly1->coef[j];

			// if don't have this term -> skip
			if(fabs(c1) < NEAR_ZERO)
				continue;

			// degree if this term
			x1 = COEFTABLE[j][0];
			y1 = COEFTABLE[j][1];
			z1 = COEFTABLE[j][2];

			// set out data
			newIndex = INDEXTABLE[x0 + x1][y0 + y1][z0 + z1];
			cout = c0*c1;

			out->coef[newIndex] += cout;
		}
	}
}

void PolynomialXYZ::multiply(PolynomialXYZ *poly0, double k, PolynomialXYZ *out){
	for(int i=0;i<PS_POLYXYZ_NTERM;i++){
		out->coef[i] = poly0->coef[i]*k;
	}
}

double PolynomialXYZ::substituteVar(double x, double y, double z){
	double result = 0;
	for(int i=0;i<PS_POLYXYZ_NTERM;i++){
		result += coef[i]*pow(x, COEFTABLE[i][0])*pow(y, COEFTABLE[i][1])*pow(z, COEFTABLE[i][2]);
	}
	return result;
}

int PolynomialXYZ::coefIndex(int xd, int yd, int zd){
	if(xd < 0 || yd < 0 || zd < 0 || xd + yd + zd > 3){
		std::cerr << "incorrect degree!\n";
	}

	return INDEXTABLE[xd][yd][zd];
}

}
