/*
 * The method of five point algorithm in this work is implemented using the algorithm from
 * "An Efficient Solution to the Five-Point Relative Pose Problem by David Nister(2004)"
 */

#ifndef _PS_FIVEPOINTALGO_H_
#define _PS_FIVEPOINTALGO_H_

#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/SVD"
#include "eigen3/unsupported/Eigen/Polynomials"

#include "../Camera/CameraParam.h"
#include "../Data/MNode.h"

#include "PolynomialXYZ.h"
#include "PolynomialZ.h"
#include "PolynomialSolver.h"
#include "../Triangulation/Triangulation.h"

#define NSELPOINTS 5

namespace ps{

class FivePointAlgo{
public:
	FivePointAlgo(){
		PolynomialXYZ::init();

		GaussJordanMat = new double*[10];
		for(int i=0;i<10;i++){
			GaussJordanMat[i] = new double[PS_POLYXYZ_NTERM];
		}
	}

	~FivePointAlgo(){
		for(int i=0;i<10;i++){
			delete[] GaussJordanMat[i];
		}
		delete[] GaussJordanMat;
	}

	//
	// adjust camera position in mNode using five-point algorithm
	//
	void adjustRelativePose(MNode *mNode0, MNode *mNode1, CameraParam *cam_param){
		// get number of points
		int n0 = mNode0->get_n_measurements();
		int n1 = mNode1->get_n_measurements();

		// calculate inverse of K matrix
		Eigen::MatrixXd K(3, 3);
		K << cam_param->fx, cam_param->s, cam_param->cx,
				0, cam_param->fy, cam_param->cy,
				0, 0, 1;
		Eigen::MatrixXd Kinv = K.inverse();

		// project pixel points of node 0 with K inverse
		Eigen::MatrixXd normPoint0(n0, MEASUREMENT_DIM + 1); //homogeneous representation
		_normalizePoints(mNode0, Kinv, normPoint0);

		// project pixel points of node 1 with K inverse
		Eigen::MatrixXd normPoint1(n1, MEASUREMENT_DIM + 1); //homogeneous representation
		_normalizePoints(mNode1, Kinv, normPoint1);

		//RANSAC for five-point algorithm loops
		Eigen::MatrixXd Q(5, 9), R(3, 3);
		Eigen::Vector3d t;
		int selectedIndex0[NSELPOINTS], selectedIndex1[NSELPOINTS];
		int selectedTag[NSELPOINTS];
		while(true){
			//TODO random common tag
			for(int i=0;i<NSELPOINTS;i++)
				selectedTag[i] = i;

			//TODO set selected indices
			for(int i=0;i<NSELPOINTS;i++)
				selectedIndex0[i] = i;

			for(int i=0;i<NSELPOINTS;i++)
				selectedIndex1[i] = i;

			//set  5x9 matrix of Q
			for(int i=0;i<NSELPOINTS;i++){
				Q.row(i) << normPoint0.row(selectedIndex0[i])(0)*normPoint1.row(selectedIndex1[i])(0),
						normPoint0.row(selectedIndex0[i])(1)*normPoint1.row(selectedIndex1[i])(0),
						normPoint0.row(selectedIndex0[i])(2)*normPoint1.row(selectedIndex1[i])(0),
						normPoint0.row(selectedIndex0[i])(0)*normPoint1.row(selectedIndex1[i])(1),
						normPoint0.row(selectedIndex0[i])(1)*normPoint1.row(selectedIndex1[i])(1),
						normPoint0.row(selectedIndex0[i])(2)*normPoint1.row(selectedIndex1[i])(1),
						normPoint0.row(selectedIndex0[i])(0)*normPoint1.row(selectedIndex1[i])(2),
						normPoint0.row(selectedIndex0[i])(1)*normPoint1.row(selectedIndex1[i])(2),
						normPoint0.row(selectedIndex0[i])(2)*normPoint1.row(selectedIndex1[i])(2);
			}

			//solve rotation & translation
			solveRelativePose(Q, R, t, mNode0->get_measurement(0), mNode1->get_measurement(0), cam_param);

			//TODO check termination criteria


			break;

		}
	}

private:
	void solveRelativePose(Eigen::MatrixXd &Q, Eigen::MatrixXd &R, Eigen::Vector3d &t, double *pixel0, double *pixel1, CameraParam *cam_param){
		// 1. Estimate null space of Q for X Y Z W (E = xX + yY + zZ + wW)
		Eigen::JacobiSVD<Eigen::MatrixXd> svdQ(Q, Eigen::ComputeFullV);
		Eigen::VectorXd vecX(9), vecY(9), vecZ(9), vecW(9);
		Eigen::MatrixXd X(3, 3), Y(3, 3), Z(3, 3), W(3, 3);
		vecX << svdQ.matrixV().col(5);
		vecY << svdQ.matrixV().col(6);
		vecZ << svdQ.matrixV().col(7);
		vecW << svdQ.matrixV().col(8);

		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				X(i, j) = vecX(3*i + j);
				Y(i, j) = vecY(3*i + j);
				Z(i, j) = vecZ(3*i + j);
				W(i, j) = vecW(3*i + j);
			}
		}

		// 2. Expansion of the cubic constraints
		PolynomialXYZ polyX, polyY, polyZ, poly1, polyE[3][3];
		PolynomialXYZ temp_xyz0, temp_xyz1, temp_xyz2;
		PolynomialXYZ constraint[10];
		PolynomialXYZ T_xyz0, T_xyz1, T_xyz2;
		polyX(1, 0, 0) = 1;
		polyY(0, 1, 0) = 1;
		polyZ(0, 0, 1) = 1;
		poly1(0, 0, 0) = 1;

		// 2.1 Construct 3x3 matrix E of polynomial
		// E = xX + yY + zZ + W
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				polyE[i][j](1, 0, 0) = X(i, j);
				polyE[i][j](0, 1, 0) = Y(i, j);
				polyE[i][j](0, 0, 1) = Z(i, j);
				polyE[i][j](0, 0, 0) = W(i, j);
			}
		}

		// 2.2 Expand constraint (5) : det(E) = 0
		// 2.2.1 First Term
		PolynomialXYZ::multiply(&polyE[0][1], &polyE[1][2], &temp_xyz0); //E12*E23
		PolynomialXYZ::multiply(&polyE[0][2], &polyE[1][1], &temp_xyz1); //E13*E22
		PolynomialXYZ::minus(&temp_xyz0, &temp_xyz1, &temp_xyz2); //E12*E23 - E13*E22
		PolynomialXYZ::multiply(&temp_xyz2, &polyE[2][0], &T_xyz0); //(E12*E23 - E13*E22)*E31

		// 2.2.2 Second Term
		PolynomialXYZ::multiply(&polyE[0][2], &polyE[1][0], &temp_xyz0); //E13*E21
		PolynomialXYZ::multiply(&polyE[0][0], &polyE[1][2], &temp_xyz1); //E11*E23
		PolynomialXYZ::minus(&temp_xyz0, &temp_xyz1, &temp_xyz2); //E13*E21 - E11*E23
		PolynomialXYZ::multiply(&temp_xyz2, &polyE[2][1], &T_xyz1); //(E13*E21 - E11*E23)*E32

		// 2.2.3 Third Term
		PolynomialXYZ::multiply(&polyE[0][0], &polyE[1][1], &temp_xyz0); //E11*E22
		PolynomialXYZ::multiply(&polyE[0][1], &polyE[1][0], &temp_xyz1); //E12*E21
		PolynomialXYZ::minus(&temp_xyz0, &temp_xyz1, &temp_xyz2); //E11*E22 - E12*E21
		PolynomialXYZ::multiply(&temp_xyz2, &polyE[2][2], &T_xyz2); //(E11*E22 - E12*E21)*E33

		// 2.2.4 Sum
		PolynomialXYZ::add(&T_xyz0, &T_xyz1, &temp_xyz0);
		PolynomialXYZ::add(&temp_xyz0, &T_xyz2, &constraint[0]);

		// 2.3 Expand constraint (6) : (E)(ET)(E) - 0.5*trace(EET)E
		// 2.3.1 construct E*ET
		PolynomialXYZ EET[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				PolynomialXYZ::multiply(&polyE[i][0], &polyE[j][0], &T_xyz0);
				PolynomialXYZ::multiply(&polyE[i][1], &polyE[j][1], &T_xyz1);
				PolynomialXYZ::multiply(&polyE[i][2], &polyE[j][2], &T_xyz2);

				PolynomialXYZ::add(&T_xyz0, &T_xyz1, &temp_xyz0);
				PolynomialXYZ::add(&temp_xyz0, &T_xyz2, &EET[i][j]);
			}
		}

		// 2.3.2 construct 0.5*trace(EET)
		PolynomialXYZ halfTraceEET;
		PolynomialXYZ::add(&EET[0][0], &EET[1][1], &temp_xyz0);
		PolynomialXYZ::add(&temp_xyz0, &EET[2][2], &temp_xyz1);
		PolynomialXYZ::multiply(&temp_xyz1, 0.5, &halfTraceEET);

		// 2.3.3 construct lambda
		PolynomialXYZ lambda[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				if(i == j){
					PolynomialXYZ::minus(&EET[i][j], &halfTraceEET, &lambda[i][j]);
				}else{
					PolynomialXYZ::multiply(&EET[i][j], 1, &lambda[i][j]);
				}
			}
		}

		// 2.3.4 construct lambda*E
		PolynomialXYZ lambdaE[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				PolynomialXYZ::multiply(&lambda[i][0], &polyE[0][j], &T_xyz0);
				PolynomialXYZ::multiply(&lambda[i][1], &polyE[1][j], &T_xyz1);
				PolynomialXYZ::multiply(&lambda[i][2], &polyE[2][j], &T_xyz2);

				PolynomialXYZ::add(&T_xyz0, &T_xyz1, &temp_xyz0);
				PolynomialXYZ::add(&temp_xyz0, &T_xyz2, &lambdaE[i][j]);
			}
		}

		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				PolynomialXYZ::multiply(&lambdaE[i][j], 1, &constraint[3*i + j + 1]);
			}
		}

		// 3. Gauss-Jordan elimination with partial pivoting on the 10x20 matrix
		// 3.1 Build Matrix for Gaussian Elimination
		for(int c=0;c<10;c++){
			for(int i=0;i<PS_POLYXYZ_NTERM;i++){
				GaussJordanMat[c][i] = constraint[c][i];
			}
		}

		// 3.2 Perform Gaussian Elimination
		_gauss_jordan(GaussJordanMat, 10, PS_POLYXYZ_NTERM);

		// 4. Extraction of roots from the tenth degree polynomial
		// 4.1 Construct B
		int xp, yp, max_zp;
		int row_front, row_back;
		PolynomialZ B[3][3];
		PolynomialZ temp_z0, temp_z1, temp_z2, Pz0, Pz1, Pz2;
		PolynomialZ constraintZ;
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				xp = (j == 0)?1:0;
				yp = (j == 1)?1:0;
				max_zp = (j==2)?3:2;


				row_front = 4 + 2*i;
				row_back = row_front + 1;

				// calculate Bij from front
				for(int zp=0;zp <= max_zp;zp++){
					B[i][j](zp) = GaussJordanMat[row_front][PolynomialXYZ::coefIndex(xp, yp, zp)];
				}

				// minus back
				for(int zp=0; zp <= max_zp; zp++){
					B[i][j](zp + 1) -= GaussJordanMat[row_back][PolynomialXYZ::coefIndex(xp, yp, zp)];
				}
			}
		}

		// 4.2 Expand constraint (14) : det(B) = 0
		// 4.2.1 First Term
		PolynomialZ::multiply(&B[0][1], &B[1][2], &temp_z0); //B12*B23
		PolynomialZ::multiply(&B[0][2], &B[1][1], &temp_z1); //B13*B22
		PolynomialZ::minus(&temp_z0, &temp_z1, &temp_z2); //B12*B23 - B13*B22
		PolynomialZ::multiply(&temp_z2, &B[2][0], &Pz0); //(B12*B23 - B13*B22)*B31

		// 4.2.2 Second Term
		PolynomialZ::multiply(&B[0][2], &B[1][0], &temp_z0); //B13*B21
		PolynomialZ::multiply(&B[0][0], &B[1][2], &temp_z1); //B11*B23
		PolynomialZ::minus(&temp_z0, &temp_z1, &temp_z2); //B13*B21 - B11*B23
		PolynomialZ::multiply(&temp_z2, &B[2][1], &Pz1); //(B13*B21 - B11*B23)*B32

		// 4.2.3 Third Term
		PolynomialZ::multiply(&B[0][0], &B[1][1], &temp_z0); //B11*B22
		PolynomialZ::multiply(&B[0][1], &B[1][0], &temp_z1); //B12*B21
		PolynomialZ::minus(&temp_z0, &temp_z1, &temp_z2); //B11*B22 - B12*B21
		PolynomialZ::multiply(&temp_z2, &B[2][2], &Pz2); //(B11*B22 - B12*B21)*B33

		// 4.2.4 Sum
		PolynomialZ::add(&Pz0, &Pz1, &temp_z0);
		PolynomialZ::add(&temp_z0, &Pz2, &constraintZ);

		// 5. Construct Essential Matrix
		// 5.1 Extract root of constraintZ
		int nRoots;
		std::vector<double> realX, realY, realZ;
		double *z_coef = new double[PS_POLYZ_NTERM];
		double *rootR = new double[PS_POLYZ_DEGREE];
		double *rootI = new double[PS_POLYZ_DEGREE];
		for(int i=0;i<PS_POLYXYZ_NTERM;i++){
			z_coef[i] = constraintZ(i);
		}

		PolynomialSolver::solvePoly(z_coef, PS_POLYZ_DEGREE, rootR, rootI);
		for(int i=0;i<10;i++){
			if(fabs(rootI[i]) < NEAR_ZERO){
				realZ.push_back(rootR[i]);
			}
		}
		nRoots = realZ.size();

		delete[] z_coef;
		delete[] rootR;
		delete[] rootI;

		// 5.2 get x and y
		double p1, p2, p3;
		for(int i=0;i<nRoots;i++){
			p1 = Pz0.substituteVar(realZ[i]);
			p2 = Pz1.substituteVar(realZ[i]);
			p3 = Pz2.substituteVar(realZ[i]);
			realX.push_back(p1/p3);
			realY.push_back(p2/p3);
		}

		// 5.3 reconstruct possible E
		std::vector<Eigen::MatrixXd> realE;
		for(int i=0;i<nRoots;i++){
			Eigen::MatrixXd tmpE(3, 3);
			tmpE = realX[i]*X + realY[i]*Y + realZ[i]*Z;
			realE.push_back(tmpE);
		}

		// 6. Recovery of R and t corresponding to each real root
		Eigen::MatrixXd EU(3, 3), EV(3, 3), ED(3, 3), posU(3, 3), posV(3, 3), D(3, 3);
		Eigen::MatrixXd transR[2];
		Eigen::Vector3d transT[2];
		double **Rdouble, tdouble[3], x_est[3];

		Rdouble = new double*[3];
		for(int i=0;i<3;i++){
			Rdouble[i] = new double[3];
		}


		D << 0, 1, 0,
			-1, 0, 0,
			0, 0, 1;

		for(int i=0;i<nRoots;i++){
			// 6.1 Calculate possible R and t
			// 6.1.1 eigen decomposition of E
			_essentialMatSVD(&realE[i], &EU, &ED, &EV);

			// 6.1.2 let U and V to have det > 0
			//d0 = ED(0, 0);
			//d1 = ED(1, 1);
			//posU.col(0) = sqrt(d0) * EU.col(0);
			//posU.col(1) = sqrt(d1) * EU.col(1);
			//if(EU.determinant() > 0){
			//	posU.col(2) = EU.col(2);
			//}else{
			//	posU.col(2) = -EU.col(2);
			//}

			//posV.col(0) = sqrt(d0) * EV.col(0);
			//posV.col(1) = sqrt(d1) * EV.col(1);
			//if(EV.determinant() > 0){
			//	posV.col(2) = EV.col(2);
			//}else{
			//	posV.col(2) = -EV.col(2);
			//}

			if(EU.determinant() > 0 && EV.determinant() > 0){
				posU << EU;
				posV << EV;
			}else{
				continue;
			}

			// 6.1.3 calculate R and t
			transR[0] = posU * D * posV.transpose();
			transR[1] = posU * D.transpose() * posV.transpose();

			transT[0] << posU(0, 2), posU(1, 2), posU(2, 2);
			transT[1] << -transT[0];

			// 6.2 check for possible R t by triangulation
			int selR = 0, selT = 0;
			for(int ri=0;ri<3;ri++){
				tdouble[ri] = transT[selT](ri);
				for(int rj=0;rj<3;rj++){
					Rdouble[ri][rj] = transR[selR](ri, rj);
				}
			}
			Triangulation::estimatePointLocation(pixel0, pixel1, Rdouble, tdouble, cam_param, x_est);
		}

		for(int i=0;i<3;i++)
			delete[] Rdouble[i];
		delete[] Rdouble;
	}

	//
	//
	//


	//
	// Solve SVD of Essential Matrix
	//
	void _essentialMatSVD(Eigen::MatrixXd *E, Eigen::MatrixXd *U, Eigen::MatrixXd *D, Eigen::MatrixXd *V){
		Eigen::JacobiSVD<Eigen::MatrixXd> svdE(*E, Eigen::ComputeFullU | Eigen::ComputeFullV);
		*D << svdE.singularValues()(0), 0, 0,
				0, svdE.singularValues()(1), 0,
				0, 0, svdE.singularValues()(2);
		*U << svdE.matrixU();
		*V << svdE.matrixV();

//		//initialize e
//		Eigen::Vector3d e[3];
//		for(int i=0;i<3;i++){
//			e[i] = E->col(i);
//		}
//
//		//1. SVD Decomposition
//
//		//findmax ea*eb
//		int max_a = 0, max_b = 1;
//		double max_norm = e[max_a].cross(e[max_b]).norm();
//
//		double new_norm = e[0].cross(e[2]).norm();
//		if(max_norm < new_norm){
//			max_a = 0;
//			max_b = 2;
//			max_norm = new_norm;
//		}
//
//		new_norm = e[1].cross(e[2]).norm();
//		if(max_norm < new_norm){
//			max_a = 1;
//			max_b = 2;
//			max_norm = new_norm;
//		}
//
//		//calculate v part
//		Eigen::Vector3d t0, t1, t2, temp;
//		temp = e[max_a].cross(e[max_b]);
//		t2 = temp/temp.norm();
//		t0 = e[max_a]/e[max_a].norm();
//		t1 = t2.cross(t0); //cross
//		V->col(0) = t0;
//		V->col(1) = t1;
//		V->col(2) = t2;
//
//		//calculate u part
//		temp = (*E) * V->col(0);
//		t0 = temp/temp.norm();
//		temp = (*E) * V->col(1);
//		t1 = temp/temp.norm();
//		t2 = t0.cross(t1);
//
//		U->col(0) = t0;
//		U->col(1) = t1;
//		U->col(2) = t2;
//
//		//calculate d part
//		Eigen::MatrixXd EV(3, 3);
//		EV = (*E) * (*V);
//		double d0 = (EV)(0, 0)/(*U)(0, 0);
//		double d1 = (EV)(0, 1)/(*U)(0, 1);
//		double d2 = (EV)(0, 1)/(*U)(0, 2);
//		*D << d0, 0, 0,
//				0, d1, 0,
//				0, 0, 0;
//
//		std::cout << "E" << std::endl;
//		std::cout << *E << std::endl;
//
//		std::cout << "E2" << std::endl;
//		std::cout << (*U) * (*D) * (*V) << std::endl;

	}

	//
	// perform Gaussian Elimination of matrix
	//
	void _gauss_jordan(double **G, int rows, int cols){
		//normalize matrix
		double row_max;
		for(int i=0;i<rows;i++){
			//find max
			row_max = fabs(G[i][0]);
			for(int j=1;j<cols;j++){
				if(row_max < fabs(G[i][j]))
					row_max = fabs(G[i][j]);
			}

			//normalize row
			for(int j=0;j<cols;j++){
				G[i][j] /= row_max;
			}
		}

		//perform elimination
		double pivot_val, front_val;
		int row_max_index;
		for(int p=0;p<rows;p++){
			// find max row to be a pivot
			row_max_index = p;
			for(int i=p + 1;i<rows;i++){
				if(fabs(G[i][p]) > fabs(G[row_max_index][p]))
					row_max_index = i;
			}

			if(fabs(G[row_max_index][p]) < NEAR_ZERO){
				std::cerr << "a variable does not contain information!" << std::endl;
			}

			// swap max row with pth row
			_swap(p, row_max_index, G, rows, cols);

			// divide all element in pivot row with pivot value
			pivot_val = G[p][p];
			for(int j=p;j<cols;j++){
				G[p][j] /= pivot_val;
			}

			// remove each other below row
			for(int i=p+1;i<rows;i++){
				front_val = G[i][p];
				for(int j=p;j<cols;j++){
					G[i][j] += -front_val*G[p][j];
				}
			}
		}
	}

	// swap pth row with qth row
	void _swap(int p, int q, double **G, int rows, int cols){
		double temp;
		for(int j=0;j<cols;j++){
			temp = G[p][j];
			G[p][j] = G[q][j];
			G[q][j] = temp;
		}
	}

	//
	// multiply each node by inverse of intrinsic matrix
	//
	void _normalizePoints(MNode *node, Eigen::MatrixXd &Kinv, Eigen::MatrixXd &normPoints){
		if(normPoints.rows() != node->get_n_measurements() || normPoints.cols() != MEASUREMENT_DIM + 1){
			std::cerr << "Incompatible normPoints dimension\n";
			return;
		}

		Eigen::Vector3d oldVect;
		for(int p=0;p<node->get_n_measurements();p++){
			oldVect << node->get_measurement(p)[0],
					node->get_measurement(p)[1],
					1;

			normPoints.row(p) = Kinv*oldVect;
		}
	}

private:
	double **GaussJordanMat;
};

}


#endif
