#ifndef _PS_MNODE_H_
#define _PS_MNODE_H_

#include<iostream>
#include<fstream>
#include "../Common/Common.h"

namespace ps{

//
// Measured Node
//
class MNode{
public:
	~MNode(){
		if(_camera_position != 0){
			delete[] _camera_position;
			delete[] _landmark_tag;
			delete[] _landmark_measurement; //array size 3*_n_measurements
		}
	}

	bool loadFile(const char *file_name){
		// open file
		std::ifstream file;
		file.open(file_name);
		if(!file.is_open()){
			std::cerr << "cannot open file " << file_name << std::endl;
			return false;
		}

		// read camera location (6 dimensions)
		_camera_position = new double[CAMERA_DIM];
		for(int i=0;i < CAMERA_DIM; i++){
			file >> _camera_position[i];
		}

		// read number of landmarks
		file >> _n_measurements;

		// read landmark [index measurement(3 dimensions each)]
		_landmark_tag = new int[_n_measurements];
		_landmark_measurement = new double[MEASUREMENT_DIM*_n_measurements];
		double *m;
		for(int i=0;i<_n_measurements; i++){
			m = get_measurement(i);
			file >> _landmark_tag[i];
			file >> m[0];
			file >> m[1];
		}

		//close file
		file.close();

		return true;
	}

	int get_n_measurements(){
		return _n_measurements;
	}

	int get_landmark_tag(int i){
		return _landmark_tag[i];
	}

	double* get_measurement(int i){
		return _landmark_measurement + MEASUREMENT_DIM*i;
	}

	double* get_camera(){
		return _camera_position;
	}

public:
	double *_camera_position;
	int _n_measurements;
	int *_landmark_tag;
	double *_landmark_measurement; //array size 3*_n_measurements

};
}


#endif
