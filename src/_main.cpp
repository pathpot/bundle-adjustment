#include <cmath>
#include <cstdio>
#include <iostream>

#include "ceres/ceres.h"
#include "ceres/rotation.h"

#include "Ply/Ply.h"
#include "BundleAdjustment/BundleAdjustment.h"
#include "FivePointAlgo/FivePointAlgo.h"


bool writeToTestError(){
	return 0;
}

//convert inplace
void EulertoAngleAxis(double *angle){

	double s1 = sin(angle[2]);
	double s2 = sin(angle[1]);
	double s3 = sin(angle[0]);
	double c1 = cos(angle[2]);
	double c2 = cos(angle[1]);
	double c3 = cos(angle[0]);

	double a = 2 * acos(c1*c2*c3 - s1*s2*s3);
	double x = s1* s2* c3 +c1* c2* s3;
	double y = s1* c2* c3 + c1* s2* s3;
	double z = c1* s2* c3 - s1 *c2 *s3;
	double norm = sqrt(x*x + y*y + z*z);

	if(norm == 0){
		angle[0] = angle[1] = angle[2] = 0;
	}else{
		angle[0] = a*x/norm;
		angle[1] = a*y/norm;
		angle[2] = a*z/norm;
	}

	//printf("%lf %lf %lf\n", angle[0], angle[1], angle[2]);
//	angle[0] = 0;
//	angle[1] = 0;
//	angle[2] = 0;

}

bool loadNodes(const char *filename, std::vector<ps::MNode*> &nodeSet){

	std::fstream ifs(filename);
	if (!ifs){
		printf("cannot open file %s\n",filename);
		return false;
	}

	std::stringstream iss;
	iss << ifs.rdbuf();
	ifs.close();

	int nNodes;
	iss >> nNodes;

	ps::MNode *node;
	for(int i=0;i<nNodes;i++){
		node = new ps::MNode();

		// read camera location (6 dimensions)
		node->_camera_position = new double[CAMERA_DIM];
		for(int i=0;i < CAMERA_DIM; i++){
			iss >> node->_camera_position[i];
			//node->_camera_position[i] = 0;
		}

		// convert first three camera position from Euler angle to angle axis
		//EulertoAngleAxis(node->_camera_position);

		// read number of landmarks
		iss >> node->_n_measurements;

		// read landmark [index measurement(3 dimensions each)]
		node->_landmark_tag = new int[node->_n_measurements];
		node->_landmark_measurement = new double[MEASUREMENT_DIM*node->_n_measurements];
		double *m;
		for(int i=0;i<node->_n_measurements; i++){
			m = node->get_measurement(i);
			iss >> node->_landmark_tag[i];
			iss >> m[0];
			iss >> m[1];
		}

		nodeSet.push_back(node);
	}
	return true;
}

int main(){
	//load camera param
	ps::CameraParam cam_param;
	cam_param.loadFile("param/cam_param.txt");

	// for each image, load landmark measurements into BNode + initialize BNode's camera position.
//	int nNodes = 5;
//	int nLandmarks = 5;
//	ps::MNode mNode[5];
//	mNode[0].loadFile("img/Board/Board01.txt");
//	mNode[1].loadFile("img/Board/Board02.txt");
//	mNode[2].loadFile("img/Board/Board03.txt");
//	mNode[3].loadFile("img/Board/Board04.txt");
//	mNode[4].loadFile("img/Board/Board05.txt");

	//ps::FivePointAlgo fivePointAlgo;
	//fivePointAlgo.adjustRelativePose(&mNode[0], &mNode[1], &cam_param);
	//return 0;

	std::vector<ps::MNode*> mNodes;
//	for(int i=0;i<nNodes;i++){
//		mNodes.push_back(&mNode[i]);
//	}

	//int nLandmarks = 54; //old
	//loadNodes("img/Board/Board.txt", mNodes); // old
	loadNodes("data/Test01/Test01Nodes.txt", mNodes); // good

	// initialize landmarks locations
	ps::BMap bMap;
	bMap.initialize("data/Test01/Test01Map.txt"); // good
	//bMap.initialize(nLandmarks); // old
	ps::Ply::write_ply("result_SLAM.ply", bMap.get_landmark(0), bMap.get_n_landmarks());
	// perform bundle adjustment
	ps::BundleAdjustment bundleAdjustment(&cam_param);
	bundleAdjustment.doBundleAdjustment(&mNodes, &bMap);
	printf("Finish Bundle Adjustment!\n");
	ps::Ply::write_ply("result_BA.ply", bMap.get_landmark(0), bMap.get_n_landmarks());
	return 0;
}
